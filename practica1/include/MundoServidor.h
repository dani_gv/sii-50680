// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
//#include "Raqueta.h"		//Esta incluido en DatosMemCompartida.h
#include "DatosMemCompartida.h"


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <string.h>

#include <pthread.h>
#include "Socket.h"
#include <iostream>
#include <signal.h>

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;

	int tuberia_logger; 
	DatosMemCompartida datos; 
	DatosMemCompartida * pdatos; 

	pthread_t th1; 
	void RecibeComandosJugador(); 
	void RecibeComandosJugador2(); 

	//Hilo para comandos del jugador 2
	pthread_t th2; 

	//Gestionar conexiones de clientes
	pthread_t thConex; 
	Socket servidor;
   	std::vector<Socket> conexiones;
   	void GestionaConexiones();

	int fin; 
	char tam_espec[200]; 

	//Gestionar conexion de un cliente
	Socket conectar; 
	Socket comunicar; 
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
