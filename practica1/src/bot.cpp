#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"

int main()	{

	DatosMemCompartida *pdatos; 
	char *org; 
	
	int a = open ("/tmp/bot.txt", O_RDWR); 

	org = static_cast <char *> (mmap(NULL, sizeof(*(pdatos)), PROT_READ | PROT_WRITE, MAP_SHARED, a, 0)); 
//	pdatos = static_cast <DatosMemCompartida *> (org); 
	pdatos = (DatosMemCompartida *) org; 
	close (a); 


	while (1)	{

		float centro_raqueta = (pdatos->raqueta1.y1 + pdatos->raqueta1.y2) / 2; 

		if (centro_raqueta < pdatos->esfera.centro.y)
			pdatos->accion = 1; 

		else if (centro_raqueta > pdatos->esfera.centro.y)
			pdatos->accion = -1; 
		
		else 
			pdatos->accion = 0; 

		usleep(25000);
	}
	munmap(org, sizeof(*(pdatos))); 
	return 0; 
}
