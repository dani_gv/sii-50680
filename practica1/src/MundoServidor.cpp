// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <signal.h>

#define TAM_BUFFER 70

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

char* org; 
void* hilo_comandos(void* d); 
void* hilo_comandos2(void* d);
void* hilo_conexiones(void* d);


//Tratamiento de señales
void tratar_alarma(int num)	{
	switch (num)	{
		case SIGINT: 
			exit (num); 
		case SIGPIPE:
			exit (num); 
		case SIGUSR1:
			exit (0); 
		case SIGTERM:
			exit (num); 
	} 
}



CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close (tuberia_logger); 
	/*close (tuberia_servidor_cliente); 
	close (tuberia_cliente_servidor_teclas);*/
	conectar.Close(); 
	comunicar.Close();  
	fin = 1; 
	pthread_join(th1, NULL);
	pthread_join(th2, NULL);
	pthread_join(thConex, NULL);

	for(int i=0;i<conexiones.size();i++){
		conexiones[i].Close();
	}
	conectar.Close();

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);

}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	sprintf(cad, "Servidor"); 
	print(cad,330,0,1,1,1); 

	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	char buf[TAM_BUFFER]; 
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		sprintf(buf, "Jugador 2 marca 1 punto, lleva un total de %d puntos", puntos2); 
		write (tuberia_logger, buf, sizeof(buf)); 
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		sprintf(buf, "Jugador 1 marca 1 punto, lleva un total de %d puntos", puntos1); 
		write (tuberia_logger, buf, sizeof(buf));
	}

	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y,jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, 	puntos2);

	comunicar.Send(cad, sizeof(cad)); 

	//Tratamiento de señales mediante el empleo de un manejador
	struct sigaction act; 
	act.sa_handler = tratar_alarma;
	act.sa_flags = 0; 
	sigaction (SIGINT,  &act, NULL);
	sigaction (SIGTERM, &act, NULL);
	sigaction (SIGPIPE, &act, NULL);
	sigaction (SIGUSR1, &act, NULL);


	//Gestiona desconexiones de clientes
	for (i=conexiones.size()-1; i>=0; i--) {
        	if (conexiones[i].Send(cad, sizeof(cad)) <= 0) {
        	    conexiones.erase(conexiones.begin()+i);
            		if (i < 2)	{
              			puntos1=0; 
				puntos2=0; 
			}
         	}
     	}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
/*
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}*/
}

void CMundo::Init()
{

	fin = 0; 

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	tuberia_logger = open ("/tmp/mififo1", O_WRONLY); 

	//Creacion del thread que sirve como hilo entre el servidor y el cliente
	pthread_create(&th1, NULL, hilo_comandos, this);
	//Hilo comandos jugador 2
	pthread_create(&th2, NULL, hilo_comandos2,this);

	//Creacion de hilo para clientes
	pthread_create(&thConex, NULL, hilo_conexiones, this); 

	char ip[] = "127.0.0.1";
	int puerto = 3550; 

	conectar.Connect(ip, puerto);
	conectar.InitServer(ip, puerto); 
	comunicar = conectar.Accept(); 
	char cad[100]; 
	comunicar.Receive(cad, strlen(cad)+1); 
	std::cout << cad << std::endl; 
}

//Codigo dado para la creacion del hilo y la recepcion de teclas
void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}

void* hilo_comandos2(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador2();
}

void* hilo_conexiones(void* f)
{
      CMundo* p=(CMundo*) f;
      p->GestionaConexiones();
}

void CMundo::GestionaConexiones()
{
	Socket con;
	int size; //numero de clientes conectados	 
	while (fin==0) {
            usleep(10);
	    conexiones.push_back(conectar.Accept());	
	    size = conexiones.size();
	    if(size==1) 
		printf("Jugador1 CONECTADO\n");
	  
	    else if (size==2)
			 printf("Jugador2 CONECTADO\n");
	    
	    else if (size > 2)
			printf("NUEVO ESPECTADOS CONECTADO\n");

	    if(conexiones.size()>2){
		for (int i=conexiones.size()-1; i>=2; i--) 
		    if(conexiones[i].Receive(tam_espec, sizeof(tam_espec))==-1)	{
			perror("Error al recibir mensaje en thread Espectadores\n Un espectador se fue\n");
			continue;
		    }
		}
	}
}

void CMundo::RecibeComandosJugador2()
{
     while (fin==0) {
            usleep(10);
            char cad[100];
       
	    conexiones[2].Receive(cad, sizeof(cad)); 
            unsigned char key;
            sscanf(cad,"%c",&key);
           
            if(key=='w')jugador1.velocidad.y=-4;
            if(key=='s')jugador1.velocidad.y=4;
      }
}

void CMundo::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[100];
       
	    conexiones[2].Receive(cad, sizeof(cad)); 
            unsigned char key;
            sscanf(cad,"%c",&key);
          
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}
