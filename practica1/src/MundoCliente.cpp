// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#define TAM_BUFFER 70

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

char* org; 
CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	munmap(org,sizeof(datos));
	/*close (tuberia_servidor_cliente); 
	unlink("/tmp/tuberia_servidor_cliente"); 
	close (tuberia_cliente_servidor_teclas); 
	unlink ("/tmp/tuberia_cliente_servidor_teclas"); */

	comunicar.Close(); 
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);

}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	sprintf(cad,"Cliente"); 
	print(cad,330,0,1,1,1); 
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	char buf[TAM_BUFFER]; 
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	}


	//Hacer que el programa acabe cuando se alcancen los 9 puntos
	if (puntos1+puntos2==9)	{  
		exit(1);  
	}

	//Control de la raqueta mediante el BOT.cpp
	pdatos->esfera = esfera; 
	pdatos->raqueta1 = jugador1; 

	if (pdatos->accion == -1)
		OnKeyboardDown ('s',0,0); 
	else if (pdatos->accion == 1)	
		OnKeyboardDown ('w',0,0);

	//Leer los datos provenientes del servidor
	char cad[200]; 
	comunicar.Receive(cad, sizeof(cad)); 
		 
		sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{	

	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':
		if (esfera.velocidad.y > 0)
			jugador1.velocidad.y= - esfera.velocidad.y;
		else
			jugador1.velocidad.y= esfera.velocidad.y;
		break;
	case 'w':
		if (esfera.velocidad.y < 0)
			jugador1.velocidad.y= - esfera.velocidad.y;
		else
			jugador1.velocidad.y=esfera.velocidad.y;
		break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}

	char cad[100]; 
	sprintf(cad, "%c", key); 
	comunicar.Send(cad, sizeof(cad)); 
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//Fichero proyectado en Memoria
	/*datos.raqueta1 = jugador1;
	datos.esfera = esfera; 
*/
	int a = open ("/tmp/bot.txt", O_CREAT | O_RDWR | O_TRUNC, 0666); 
	if (a < 0)
		perror ("Error al abrir el fichero"); 

	else {
		write (a, &datos, sizeof(datos)); 
		org = static_cast <char *> (mmap(NULL, sizeof(datos), PROT_READ | PROT_WRITE, MAP_SHARED, a, 0)); 
		close (a); 
		//pdatos = static_cast<DatosMemCompartida *> (org); 
		pdatos = (DatosMemCompartida *) (org); 
	}
/*
	//Creacion Tubería para relacionarse con el servidor LEYENDO
	mkfifo("/tmp/tuberia_servidor_cliente", 0777);
	tuberia_servidor_cliente = open ("/tmp/tuberia_servidor_cliente", O_RDONLY); 

	//Creacion tuberia para relacionarse con el servidor ESCRIBIENDO directamente en el 
	mkfifo("/tmp/tuberia_cliente_servidor_teclas", 0777); 
	tuberia_cliente_servidor_teclas = open ("/tmp/tuberia_cliente_servidor_teclas", O_WRONLY); 
*/
	char cad[100]; 
	std::cout << "Introduzca su nombre: "<< std::endl; 
	std::cin >> cad; 

	char ip[] = "127.0.0.1";
	int puerto = 3550; 
	comunicar.Connect(ip, puerto); 
	comunicar.Send(cad, sizeof(cad)); 
}
