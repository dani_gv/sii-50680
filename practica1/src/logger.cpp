#include <sys/types.h>
#include <sys/stat.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>
#include <stdlib.h>

using namespace std; 

#define TAM_BUFFER 70

int main()	{
	mkfifo ("/tmp/mififo1", 0777); 
	int fd = open ("/tmp/mififo1", O_RDONLY); 
	int a; 

	char buf[TAM_BUFFER]; 
	while (1)	{

		a = read(fd, buf, sizeof(buf)); 
		if (a!=70)	{
			cout<<"Fin del juego"<<endl; 
			break;  	
		}

		else
			cout<<buf<<endl; 
	}

	close (fd); 
	unlink("/tmp/mififo1"); 

	return 0; 
}
